" dein.vim setup
" As per https://github.com/Shougo/dein.vim#unixlinux-or-mac-os-x
if &compatible
  set nocompatible
endif
" Add the dein installation directory into runtimepath
set runtimepath+=~/.cache/dein/repos/github.com/Shougo/dein.vim

if dein#load_state('~/.cache/dein')
  call dein#begin('~/.cache/dein')

  call dein#add('~/.cache/dein/repos/github.com/Shougo/dein.vim')
  call dein#add('Shougo/deoplete.nvim')
  call dein#add('scrooloose/nerdtree')

  if !has('nvim')
    call dein#add('roxma/nvim-yarp')
    call dein#add('roxma/vim-hug-neovim-rpc')
  endif

  call dein#end()
  call dein#save_state()
endif

filetype plugin indent on
syntax enable

" Personal setup
set tabstop=8 softtabstop=0 expandtab shiftwidth=4 smarttab
set number

" Highlight alienfile as Perl
autocmd BufNewFile,BufRead alienfile set syntax=perl

" Show trailing spaces, etc
set list
set listchars=tab:▸\ ,trail:·,nbsp:␣

" Custom mappings
nmap <C-x> :NERDTreeToggle<CR>

" Strip trailing whitespace from selected filetypes
fun! TrimWhitespace()
    let l:save = winsaveview()
    keeppatterns %s/\s\+$//e
    call winrestview(l:save)
endfun

autocmd FileType c,cpp,java,php,ruby,python,perl autocmd BufWritePre * :call TrimWhitespace()

" Highlight max line length
let &colorcolumn=join(range(81,999),",")
highlight ColorColumn ctermbg=235 guibg=#2c2d27
